#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Count.h"


Count* cnt_init(Count* this, const char* name) {
    this->numChars = 0;
    this->numWords = 0;
    this->numLines = 0;
    this->name     = name;
    return this;
}

static unsigned wordsOf(char* s) {
    const char* delim = " ";
    unsigned n = 0;
    for (char* p = strtok(s, delim); p != NULL; p = strtok(NULL, delim)) ++n;
    return n;
}

Count* cnt_add(Count* this, char* line) {
    this->numLines++;
    this->numChars += strlen(line);
    this->numWords += wordsOf(line);
    return this;
}

Count* cnt_sum(Count* this, Count* that) {
    this->numLines += that->numLines;
    this->numWords += that->numWords;
    this->numChars += that->numChars;
    return this;
}

Count* cnt_print(Count* this) {
    printf("%6d %8d %10d %s\n",
           this->numLines, this->numWords, this->numChars, this->name
    );
    return this;
}
