#include <stdio.h>

typedef struct {
    int   balance;
    float rate;
} Account;

void account_print(Account* self);

Account* account_init(Account* self, int balance, float rate);

void account_final_balance(Account* self);

void account_initial_balance(Account* self);

typedef void (* AccountFunction)(Account*);


int main() {
    Account acc;
    account_init(&acc, 1000, 3.5);
    account_print(&acc);
    account_final_balance(&acc);
    account_print(&acc);
    account_initial_balance(&acc);
    account_print(&acc);

    AccountFunction arr[] = {
            &account_final_balance,
            &account_initial_balance,
    };
    const unsigned N = sizeof(arr) / sizeof(arr[0]);

    printf("----------\n");
    account_init(&acc, 10000, 2.5);
    account_print(&acc);
    for (unsigned k = 0; k < N; ++k) {
        (*arr[k])(&acc);
        account_print(&acc);
    }

    return 0;
}


Account* account_init(Account* self, int balance, float rate) {
    self->balance = balance;
    self->rate    = rate;
    return self;
}

void account_print(Account* self) {
    printf("Account{SEK %d, %.2f%%}\n", self->balance, self->rate);
}

void account_final_balance(Account* self) {
    self->balance *= (1 + self->rate / 100);
}

void account_initial_balance(Account* self) {
    self->balance /= (1 + self->rate / 100);
}
