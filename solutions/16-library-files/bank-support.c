#include <stdio.h>
#include "List.h"
#include "bank-support.h"

void populate(List* accounts, int numAccounts) {
    InterestRate   rates[] = {0.5, 1.5, 2, 2.5, 3};
    const unsigned R       = sizeof(rates) / sizeof(rates[0]);

    while (--numAccounts >= 0) {
        Balance      amount     = 50 + (numAccounts * 100) % 10000;
        InterestRate percentage = rates[numAccounts % R];

        list_push(accounts, account_new(amount, percentage));
    }
}

void print(List* accounts) {
    int accno = 1;
    for (Node* node = accounts->first; node != NULL; node = node->next) {
        printf("%4d) ", accno++);
        account_print(node->data);
    }
}

void drain(List* accounts) {
    while (!list_empty(accounts)) {
        account_dispose(list_pop(accounts));
    }
}

void apply(List* accounts, AccountTransformer f) {
    for (Node* node = accounts->first; node != NULL; node = node->next) {
        (*f)(node->data);
    }
}

