extern int printf(const char*, ...);
extern int atoi(const char*);

#define MAX(a,b)    (a >= b ? a : b)

int main(int n, char* args[]) {
    int x = 42, y = 64;
    if(n >= 2) x = atoi(args[1]);
    if(n >= 3) y = atoi(args[2]);

    // *** Usage of MAX(a,b)
    printf("max(%d, %d) = ", x+1, y+1);
    printf("%d\n", MAX(++x, ++y));

    return 0;
}
