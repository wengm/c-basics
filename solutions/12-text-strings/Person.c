

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "Person.h"

Person* person_alloc() {
    return (Person*) calloc(1, sizeof(Person));
}

void person_dispose(Person* this) {
    free((void*) this);
}

Person* person_init(Person* this, const char* name, int age) {
    this->name = strdup(name);
    this->age = age;
    return this;
}

Person* person_finit(Person* this) {
    free(this->name);
    return this;
}

Person* person_new(const char* name, int age) {
    return person_init(person_alloc(), name, age);
}

void person_delete(Person* this) {
    person_dispose(person_finit(this));
}



char* person_getName(Person* this) {
    return this->name;
}

int person_getAge(Person* this) {
    return this->age;
}

char* person_setName(Person* this, const char* newName) {
    free(this->name);
    this->name = strdup(newName);
    return this->name;
}

int person_setAge(Person* this, int newAge) {
    this->age = newAge;
    return this->age;
}

char* person_toString(Person* this) {
    static char buf[128];
    sprintf(buf, "person{%s, %d}", person_getName(this), person_getAge(this));
    return buf;
}

