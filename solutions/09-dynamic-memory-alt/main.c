#include <stdio.h>
#include <stdlib.h>
#include "Account.h"

int main() {
    {
        Account* acc = (Account*) calloc(1, sizeof(Account));
        account_init(acc, 1000, 1.5);
        account_print(acc);
        free(acc);
    }

    printf("------------\n");
    {
        printf("Give size: ");
        unsigned N;
        scanf("%d", &N);

        Account* arr = (Account*) calloc(N, sizeof(Account));
        for (unsigned k = 0; k < N; ++k)
            account_init(&arr[k], (k + 1) * 100, (float) ((k + 1) * 0.25));
        for (unsigned k = 0; k < N; ++k)
            account_print(&arr[k]);
        free(arr);
    }

    return 0;
}