#!/usr/bin/env bash
set -eu

SRC=./src
OUT=./out

LIB_MEMBERS="Node List Account"
LIB_NAME=account
LIB_HDR="${SRC}/lib/hdrs"
LIB_IMP="${SRC}/lib/impl"
LIB_FILE="lib${LIB_NAME}.a"
LIB_DIR="${OUT}/lib"
LIB_OBJ="${LIB_DIR}/objs"

APP_MEMBERS="Bank"
APP_NAME=bank
APP_IMP="${SRC}/app/impl"
APP_DIR="${OUT}/app"
APP_OBJ="${APP_DIR}/objs"

CFLAGS="-std=c99 -g -Wall -Wextra -Wpedantic -Werror -Wfatal-errors -Wno-unused-variable"
INCL="-I${LIB_HDR}"

set -x
rm -rf ${OUT}

echo "--- Building LIB ---"
mkdir -p ${LIB_OBJ}
for f in ${LIB_MEMBERS}
do
    gcc ${CFLAGS} ${INCL} -c "${LIB_IMP}/${f}.c" -o "${LIB_OBJ}/${f}.o"
done

mkdir -p ${LIB_DIR}
ar crs ${LIB_DIR}/${LIB_FILE} ${LIB_OBJ}/*.o

echo "--- Building APP ---"
mkdir -p ${APP_OBJ}
for f in ${APP_MEMBERS}
do
    gcc ${CFLAGS} ${INCL} -c "${APP_IMP}/${f}.c" -o "${APP_OBJ}/${f}.o"
done

mkdir -p ${APP_DIR}
gcc ${CFLAGS} ${APP_OBJ}/*.o -o ${APP_DIR}/${APP_NAME} -L${LIB_DIR} -l${LIB_NAME}

echo "--- APP Stats ---"
tree ${OUT}
ar tv ${LIB_DIR}/${LIB_FILE}
nm -s ${LIB_DIR}/${LIB_FILE}
ldd ${APP_DIR}/${APP_NAME}
strings ${APP_DIR}/${APP_NAME} | grep 'Build-Info'

echo "--- Running APP ---"
${APP_DIR}/${APP_NAME} 10

