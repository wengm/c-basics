C Basics, 4 days
====

Welcome to this course.
The syllabus can be find at
[c/c-basics](https://www.ribomation.se/c/c-basics.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation

    git clone https://gitlab.com/ribomation-courses/c/c-basics.git
    cd c-basics

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have access to a C compiler running on preferably a Linux system, such as Ubuntu. Go for one of the listed solutions below.

* **Already have Linux installed on your laptop**<br/>
Then you are ready for the course. If it is not Ubuntu, then there might be some differences, but as long as you can handle it and do the translation yourself, there is no problem.
* **Already have access to a remote Linux system**<br/>
Same as above.
* **Is running Windows 10 on your laptop**<br/>
One of the biggest news of the update named "Aniversary Edition" released last summer, was that Windows 10 has support for running native Ubuntu Linux, which is called WSL (Windows Subsystem for Linux). You just have to enabled it. Follow the links below to proceed.
* **Otherwise**<br/>
You have to install VirtualBox and install Ubuntu into a VM. Follow the links below to proceed.

In addition, your need a C compiler such as GCC and some way to edit your program code, such as a decent text editor or a full blown IDE. Read more below for suggestions.

Installing Ubuntu @ VBox
----

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>

Installing WSL @ Windows 10
----

1. How to Install and Use the Linux Bash Shell on Windows 10<br/>
    <https://docs.microsoft.com/en-us/windows/wsl/install-win10/>
1. Once successfully installed WSL, you perhaps also want to have a handy right-click menu item of "Ubuntu Here"? Here is how to do it:
    <http://winaero.com/blog/add-bash-to-the-folder-context-menu-in-windows-10/>
1. If you would like to launch graphical applications from WSL, i.e. X-Windows apps, then you need to install a X server for Windows, such as VcXsrv, Cygwin-X or similar. <br/>
  * <https://sourceforge.net/projects/vcxsrv/>
  * <http://www.pcworld.com/article/3055403/windows/windows-10s-bash-shell-can-run-graphical-linux-applications-with-this-trick.html>
1. Once you have an X-Windows server running on Windowes, you need to define the DISPLAY environment variable  in your ~/.bashrc file : `export DISPLAY=:0`

Installing GCC @ Ubuntu
----

Install the GCC C/C++ compiler in Ubuntu

    sudo apt-get install gcc

N.B. the sudo command will prompt you for your Ubuntu login password

Suggestions for editor/ide
----

You also need to use a text editor or IDE to write your programs. If you are already familiar with tools like Emacs or Eclipse/C/C++, please go ahead and install them too. On the other hand, if you would like some advise I do recommend to choose one of these two suggestions:

* If you just want a decent text editor, then go for *Text Editor (gedit)*. It's already installed on Ubuntu and you can launch from the start menu in the upper left corner or from a terminal window with the command: <br/>
`gedit my-file.c &`

* If you want to run a good IDE instead, then download and install the trial version of *JetBrains CLion*. This is my choice of C IDE and I will be using it during the course. CLion installed to Windows can be connected to a Ubuntu @ WSL. Read more about it below:
  * <https://www.jetbrains.com/clion/>
  * <https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html>


* Another, interesting IDE/smart-editor is *MS Visual Code*, which exists for Linux as well. Even VisualCode can be connected to WSL.
  * <https://code.visualstudio.com/>
  * <https://stackoverflow.com/questions/44450218/how-do-i-use-bash-on-ubuntu-on-windows-wsl-for-my-vs-code-terminal>


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
